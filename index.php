<?php

try {
$pdo = new PDO('mysql:host=localhost;dbname=database_from_pdo;carset=utf8','root','');
$pdo->getAttribute(PDO::ATTR_ERRMODE); 

$sql= "SELECT * FROM users";
$result = $pdo->query($sql);

//1 cпособ
//print_r($result); 

//2 способ
//foreach($result as $row){
//    echo "<p>".$row['id']."||".$row['username']."||".$row['password'];
//}

//3 способ
//while($row = $result->fetch(PDO::FETCH_ASSOC)){
//    echo "<p>".$row['id']."/".$row['username']."/".$row['password'];
//}

//4 способ
//while($row = $result->fetch(PDO::FETCH_NUM)){
//    echo "<p>".$row[0]."|".$row[1]."|".$row[2];
//}

//5 способ
//while($row = $result->fetch(PDO::FETCH_BOTH)){
//    echo "<p>".$row[0]."\\".$row['username']."\\".$row[2];
//}

//6 способ
while($row = $result->fetch(PDO::FETCH_OBJ)){
    echo "<p>".$row->id."!".$row->username."!".$row->password;
}

}

catch (Exception $e) {
    echo $e->getMessage();
}


//,PDO::ERRMODE_EXCEPTION